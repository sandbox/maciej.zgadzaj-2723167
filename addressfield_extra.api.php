<?php

/**
 * @file
 * API documentation for Addressfield Extra.
 */

/**
 * Allows modules to alter the predefined US counties.
 *
 * @param array $counties
 *   The array of all predefined US counties.
 *
 * @see addressfield_extra_get_counties()
 */
function hook_addressfield_extra_counties_alter(&$counties) {
  // Add counties for Armed Forces Pacific.
  $counties['US']['AP']['373'] = t('Adams');
  $counties['US']['AP']['507'] = t('Brazoria');
  $counties['US']['AP']['208'] = t('Fulton');
  $counties['US']['AP']['337'] = t('Harford');
  $counties['US']['AP']['536'] = t('New Castle');
}
