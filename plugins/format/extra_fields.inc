<?php

/**
 * @file
 * The default format for extra address fields.
 */

$plugin = array(
  'title' => t('Extra address fields'),
  'format callback' => 'addressfield_extra_format_generate',
  'type' => 'address',
  'weight' => -99,
);

/**
 * Format generation callback.
 *
 * @see CALLBACK_addressfield_format_callback()
 */
function addressfield_extra_format_generate(&$format, $address, $context = array()) {
  if ($context['mode'] == 'form') {
    // Load the predefined address format for the selected country.
    module_load_include('inc', 'addressfield', 'addressfield.address_formats');
    $address_format = addressfield_get_address_format($address['country']);

    if (isset($context['instance']['widget']['settings']['extra_fields'])) {
      $settings = array_filter($context['instance']['widget']['settings']['extra_fields']);
    }
    else {
      $settings = array();
    }

    if (empty($settings) || !empty($settings['address_3'])) {
      $format['street_block']['address_3'] = array(
        '#type' => 'textfield',
        '#title' => t('Address 3'),
        '#size' => 30,
        '#maxlength' => 255,
        '#attributes' => array('class' => array('address-3')),
        '#tag' => 'span',
        '#default_value' => isset($address['address_3']) ? $address['address_3'] : '',
      );
    }

    if (empty($settings) || !empty($settings['address_4'])) {
      $format['street_block']['address_4'] = array(
        '#type' => 'textfield',
        '#title' => t('Address 4'),
        '#size' => 30,
        '#maxlength' => 255,
        '#attributes' => array('class' => array('address-4')),
        '#tag' => 'span',
        '#default_value' => isset($address['address_4']) ? $address['address_4'] : '',
      );
    }

    if (empty($settings) || !empty($settings['county'])) {
      $format['locality_block']['county'] = array(
        '#type' => 'select',
        '#title' => t('County'),
        '#attributes' => array('class' => array('county')),
        '#tag' => 'span',
        '#default_value' => isset($address['county']) ? $address['county'] : '',
        '#required' => in_array('county', $address_format['required_fields']),
        '#access' => in_array('county', $address_format['used_fields']),
      );

      if (!empty($format['locality_block']['county']['#access'])) {
        // Set the predefined administrative areas, if found.
        module_load_include('inc', 'addressfield_extra', 'addressfield_extra.counties');
        $counties = addressfield_extra_get_counties($address['country'], $address['administrative_area']);
        $format['locality_block']['county']['#options'] = array('' => t('- Select -')) + $counties;
      }

      $format['locality_block']['administrative_area']['#ajax'] = array(
        'callback' => 'addressfield_standard_widget_refresh',
        'wrapper' => $format['#wrapper_id'],
      );

      $format['locality_block']['postal_code']['#weight'] = 10;
    }
  }
  else {
    // Add our own render callback for the format view
    $format['#pre_render'][] = '_addressfield_extra_render_address';
  }
}
